# -*- coding=utf-8 -*-

import string, random, os.path
from flask import Flask, render_template, request, redirect, session, url_for, abort

app = Flask(__name__)
app_url = '/markiem1/shortener'
app.secret_key = 'dasoaubgsidbfiuar31b4iuqenandosjidieoqqi'

credentials = {
    'admin': 'admin123'
}

links = {'admin': {'XLDTY6SJ' : 'http://google.com'}}


# region Helpers
def id_generator(size=8, chars=string.ascii_uppercase + string.digits):
    generated = ''.join(random.choice(chars) for _ in range(size))
    if generated in links:
        return id_generator(size, chars)
    return generated


def check_login(username, password):
    if username in credentials:
        if credentials[username] == password:
            session['username'] = username
            return True
    return False


def add_link(username, href):
    id = id_generator(8)
    links[username][id] = href
    return id


def is_logged_in():
    if 'username' in session:
        return True
    return False


def is_url_active(href):
    url = request.url_root[:-1] + url_for(href)
    return url == request.url


@app.context_processor
def view_helpers():
    return dict(logged_in=is_logged_in, url_active=is_url_active)


# endregion


# region Routes
@app.route(app_url + '/')
def index():
    return redirect(url_for('homepage'), 301)


@app.route(app_url + '/login', methods=['GET', 'POST'])
def login():
    if is_logged_in():
        return redirect(url_for('homepage'))

    if request.method == 'POST':
        if check_login(request.form['username'], request.form['password']):
            return redirect(url_for('homepage'))

    return render_template('login.html')


@app.route(app_url + '/logout')
def logout():
    if is_logged_in():
        session.clear()
    return redirect(url_for('login'))


@app.route(app_url + '/home')
def homepage():
    if is_logged_in():
        return render_template('homepage.html', skrocone=links[session['username']])
    return redirect(url_for('login'))


@app.route(app_url + '/shorten', methods=['POST'])
def shorten():
    if not is_logged_in():
        return redirect(url_for('login'))
    href = request.form['href']
    add_link(session['username'], href)
    return redirect(url_for('homepage'))


@app.route(app_url + '/r/<href>')
def redirection(href):
    for user in links:
        if href in links[user]:
            return redirect(links[user][href], 301)
    abort(404)


@app.route(app_url + '/static/<resource>')
def static_resource(resource):
    path = app.root_path + '/static/' + resource
    if os.path.isfile(path):
        return open(path)
    abort(404)


# endregion

@app.errorhandler(404)
def not_found(e):
    return render_template('error_404.html'), 404


if __name__ == '__main__':
    app.run()
