# -*- coding=utf-8 -*-
import os
import unittest
import tempfile
from shortener import app


class ShortenerTestCase(unittest.TestCase):
    def setUp(self): pass

    def tearDown(self): pass

    def login(self, username, password):
        r = app.post('/login')
        assert b'https://google.com' in r.data

    def test_homepage(self):
        self.login()
        r = app.get('/skroc')
        # assert
